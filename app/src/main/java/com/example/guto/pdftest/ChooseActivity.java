package com.example.guto.pdftest;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.Guideline;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;

public class ChooseActivity extends AppCompatActivity {

    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.guideline)
    Guideline guideline;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.button3)
    Button button3;
    @BindView(R.id.button4)
    Button button4;
    @BindView(R.id.chooselabel)
    TextView chooselabel;
    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.button5)
    Button button5;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.button6)
    Button button6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_choose);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("PDF Libraries");

        ArrayList<String> list = new ArrayList<>();
        list.add("PDF sample 1 - 1 MB");
        list.add("Folha SP - 14 MB");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,list );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((MyApp)getApplicationContext()).setFile(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick({R.id.button, R.id.button2, R.id.button3, R.id.button4, R.id.button5, R.id.button6})
    public void onViewClicked(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.button:
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("file", false);
                startActivity(intent);
                break;
            case R.id.button2:
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("file", true);
                startActivity(intent);
                break;
            case R.id.button3:
                intent = new Intent(this, VigerActivity.class);
                intent.putExtra("file", false);
                startActivity(intent);
                break;
            case R.id.button4:
                intent = new Intent(this, VigerActivity.class);
                intent.putExtra("file", true);
                startActivity(intent);
                break;

            case R.id.button5:
                intent = new Intent(this, PdfRendererActivity.class);
                intent.putExtra("file", true);
                startActivity(intent);
                break;
            case R.id.button6:
                intent = new Intent(this, PDFViewPagerActivity.class);
                intent.putExtra("file", true);
                startActivity(intent);
                break;

        }
    }
}
