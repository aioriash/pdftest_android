package com.example.guto.pdftest;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.github.barteksc.pdfviewer.util.FileUtils;
import com.necistudio.vigerpdf.VigerPDF;
import com.necistudio.vigerpdf.adapter.VigerAdapter;
import com.necistudio.vigerpdf.manage.OnResultListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VigerActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.progress)
    LinearLayout progress;
    private ArrayList<Bitmap> itemData;
    private VigerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viger);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Viger");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        itemData = new ArrayList<>();
        adapter = new VigerAdapter(getApplicationContext(),itemData);
        viewPager.setAdapter(adapter);

        if(getIntent().getBooleanExtra("file", false)){
            fromFile("sample2.pdf");
        }else{
            fromNetwork("https://www.iso.org/files/live/sites/isoorg/files/archive/pdf/en/annual_report_2009.pdf");
        }
    }


    private void fromNetwork(String endpoint) {
        new VigerPDF(this, endpoint).initFromFile(new OnResultListener() {
            @Override
            public void progressData(int i) {

            }


            @Override
            public void failed(Throwable throwable) {
                Log.d("error", throwable.getMessage());

            }


            @Override
            public void resultData(Bitmap data) {
                progress.setVisibility(View.GONE);
                itemData.add(data);
                adapter.notifyDataSetChanged();
            }
        });
    }


    private void fromFile(String path) {
        try {
            File file = FileUtils.fileFromAsset(this, "sample2.pdf");
            //        File file = new File(path);
            new VigerPDF(this, file).initFromFile(new OnResultListener() {

                @Override
                public void resultData(Bitmap data) {
                    progress.setVisibility(View.GONE);

                    itemData.add(data);
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void progressData(int i) {

                }

                @Override
                public void failed(Throwable throwable) {

                }


            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
