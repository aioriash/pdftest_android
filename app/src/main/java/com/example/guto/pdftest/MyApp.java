package com.example.guto.pdftest;

import android.app.Application;

public class MyApp extends Application {
    public int selectedFile = 0;


    public String getSelectedFileName(){
        String name = "sample1.pdf";
        switch (selectedFile){
            case 0:
                name = "sample1.pdf";
                break;

            case 1:
                name = "sample2.pdf";

                break;

            case 2:
                break;
        }
        return name;
    }

    public String getSelectedFileURL(){
        String url = "https://www.adobe.com/content/dam/acom/en/devnet/acrobat/pdfs/pdf_open_parameters.pdf";
        switch (selectedFile){
            case 0:
                url = "https://www.adobe.com/content/dam/acom/en/devnet/acrobat/pdfs/pdf_open_parameters.pdf";
                break;

            case 1:
                url = "https://aioria.com.br/folha_sp.pdf";
                break;

            case 2:
                break;
        }
        return url;
    }

    public void setFile(int pos){
        selectedFile = pos;

    }
}
