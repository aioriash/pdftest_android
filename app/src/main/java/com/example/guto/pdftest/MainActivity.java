package com.example.guto.pdftest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.listener.OnRenderListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.github.barteksc.pdfviewer.util.FitPolicy;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements OnPageChangeListener {


    @BindView(R.id.pdfView)
    PDFView pdfView;
    @BindView(R.id.progress)
    LinearLayout progress;
    ProgressDialog mProgressDialog;
    public boolean fromFile = false;
    FitPolicy fp = FitPolicy.WIDTH;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setTitle("Android PDF Viewer");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fromFile = getIntent().getBooleanExtra("file", false);

        if(((MyApp)getApplication()).selectedFile == 0){
            fp = FitPolicy.BOTH;
        }else {
            fp = FitPolicy.HEIGHT;

//            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                fp = FitPolicy.HEIGHT;
//
//            }
        }

//
//        pdfView.fromUri(Uri.parse("https://www.ets.org/Media/Tests/GRE/pdf/gre_research_validity_data.pdf"))
//
//                .enableSwipe(true) // allows to block changing pages using swipe
//                .swipeHorizontal(false)
//                .enableDoubletap(true)
//                .defaultPage(0)
//                // allows to draw something on the current page, usually visible in the middle of the screen
////                .onDraw(onDrawListener)
//                // allows to draw something on all pages, separately for every page. Called only for visible pages
////                .onDrawAll(onDrawListener)
////                .onLoad(onLoadCompleteListener) // called after document is loaded and starts to be rendered
////                .onPageChange(onPageChangeListener)
////                .onPageScroll(onPageScrollListener)
////                .onError(onErrorListener)
////                .onPageError(onPageErrorListener)
////                .onRender(onRenderListener) // called after document is rendered for the first time
//                // called on single tap, return true if handled, false to toggle scroll handle visibility
////                .onTap(onTapListener)
////                .onLongPress(onLongPressListener)
//                .enableAnnotationRendering(false) // render annotations (such as comments, colors or forms)
//                .password(null)
//                .scrollHandle(null)
//                .enableAntialiasing(true) // improve rendering a little bit on low-res screens
//                // spacing between pages in dp. To define spacing color, set view background
//                .spacing(0)
////                .autoSpacing(false) // add dynamic spacing to fit each page on its own on the screen
////                .linkHandler(DefaultLinkHandler)
////                .pageFitPolicy(FitPolicy.WIDTH)
////                .pageSnap(true) // snap pages to screen boundaries
////                .pageFling(false) // make a fling change only a single page like ViewPager
////                .nightMode(false) // toggle night mode
//                .load();

        if(fromFile){


            pdfView.fromAsset(((MyApp)getApplication()).getSelectedFileName())
                    .scrollHandle( new DefaultScrollHandle(MainActivity.this){

                    } )
//                    .spacing(10)
                    .enableSwipe(true)

                    .swipeHorizontal(true)
                    .enableDoubletap(true)
                    .autoSpacing(true)

//                    .nightMode(true)

                    .pageFitPolicy(fp)
                    .pageSnap(true)
                    .enableAnnotationRendering(true)
                    .enableAntialiasing(true)
                    .onPageChange(MainActivity.this)
                    .onRender(new OnRenderListener() {
                        @Override
                        public void onInitiallyRendered(int nbPages) {

                            progress.setVisibility(View.GONE);
                            pdfView.setVisibility(View.VISIBLE);
                        }
                    })
                    .load();

            pdfView.setMinZoom(1F);
            pdfView.setMidZoom(2.5F);
            pdfView.setMaxZoom(6F);

        }else{
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage("Downloading...");
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            mProgressDialog.setCancelable(false);


//            new RetrivePDFStream().execute("https://www.iso.org/files/live/sites/isoorg/files/archive/pdf/en/annual_report_2009.pdf");

            final DownloadTask downloadTask = new DownloadTask(this);
            downloadTask.execute(((MyApp)getApplication()).getSelectedFileURL() );
            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    downloadTask.cancel(true);
                }
            });
        }
    }

    @Override
    public void onPageChanged(int page, int pageCount) {

    }


    class RetrivePDFStream extends AsyncTask<String, Void, InputStream> {
        @Override
        protected InputStream doInBackground(String... strings) {
            InputStream inputStream = null;
            try {
                URL uri = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) uri.openConnection();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = new BufferedInputStream(urlConnection.getInputStream());
                }
            } catch (IOException e) {
                return null;
            }
            return inputStream;
        }

        @Override
        protected void onPostExecute(InputStream inputStream) {



            pdfView.fromStream(inputStream)
                    .swipeHorizontal(true)
//                    .enableAnnotationRendering(true)
//                    .enableAntialiasing(true) // improve rendering a little bit on low-res screens
                    .pageSnap(true)
                    .autoSpacing(true)
                    .pageFling(true)
                    .onRender(new OnRenderListener() {
                        @Override
                        public void onInitiallyRendered(int nbPages) {
                            progress.setVisibility(View.GONE);
                            pdfView.setVisibility(View.VISIBLE);
                        }
                    })
                    .load();
//progressBar.setVisibility(View.GONE);
        }
    }



    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;

        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            mProgressDialog.show();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.setMax(100);
            mProgressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            mProgressDialog.dismiss();
            pdfView.fromFile(new File(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "/downloaded_pdf.pdf"))
                    .scrollHandle( new DefaultScrollHandle(MainActivity.this){

                    } )
//                    .spacing(10)
                    .enableSwipe(true)

                    .swipeHorizontal(true)
                    .enableDoubletap(true)
                    .autoSpacing(true)
//                    .nightMode(true)

                    .pageFitPolicy(fp)
                    .pageSnap(true)
                    .enableAnnotationRendering(true)
                    .enableAntialiasing(true)
                    .onPageChange(MainActivity.this)
                    .onRender(new OnRenderListener() {
                        @Override
                        public void onInitiallyRendered(int nbPages) {

                            progress.setVisibility(View.GONE);
                            pdfView.setVisibility(View.VISIBLE);
                        }
                    })
                    .load();

            pdfView.setMinZoom(1F);
            pdfView.setMidZoom(2.5F);
            pdfView.setMaxZoom(6F);

            if (result != null)
                Toast.makeText(context,"Download error: "+result, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context,"File downloaded", Toast.LENGTH_SHORT).show();
        }



        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();
                output = new FileOutputStream(context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) + "/downloaded_pdf.pdf");

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        deleteFolder();
        super.onDestroy();
    }

    public void deleteFolder(){
//        File dir = new File(Environment.getExternalStorageDirectory()+"Dir_name_here");
        File dir = new File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS) +  "/downloaded_pdf.pdf" );
//        if (!dir.exists()){
//            dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + issue.dbID);
//        }

        if (dir.isDirectory())
        {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++)
            {
                new File(dir, children[i]).delete();
            }
        }
        dir.delete();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
